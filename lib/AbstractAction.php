<?php

namespace Keszei\Action;

use Keszei\Action\Model\Action;

abstract class AbstractAction implements Action {

	/**
	 * @var Responder
	 */
	private $responder;

	public function __construct(Responder $responder) {
		$this->responder = $responder;
	}

	public function getResponder() {
		return $this->responder;
	}

}
