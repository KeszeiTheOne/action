<?php

namespace Keszei\Action;

use Keszei\Action\Exception\MissingResponse;
use Keszei\Action\Model\Response;

class Responder {

	/**
	 * @var Response
	 */
	private $response;

	public function getResponse() {
		if (null === $this->response) {
			throw new MissingResponse;
		}

		return $this->response;
	}

	public function setResponse($response) {
		$this->response = $response;
	}

}
