<?php

namespace Keszei\Action;

use Keszei\Action\Exception\ActionNotFound;
use Keszei\Action\Model\Action;
use Keszei\Action\Model\ActionFactory;
use Keszei\Action\Model\Config;
use Keszei\Action\Model\Request;
use Keszei\Action\Model\Response;

class ActionRegistry {

	/**
	 *
	 * @var ActionFactory[]
	 */
	private $actionFactories = [];

	/**
	 * @var Config 
	 */
	private $config;

	public function __construct(Config $config, $actionFactories) {
		$this->config = $config;

		foreach ($actionFactories as $name => $actionFactory) {
			$this->addActionFactory($name, $actionFactory);
		}
	}

	public function addActionFactory($name, ActionFactory $actionFactory) {
		$this->actionFactories[$name] = $actionFactory;
	}

	/**
	 * @return Response
	 */
	public function runAction($name, Request $request) {
		$action = $this->createAction($responder = new Responder(), $name);
		$action->run($request);

		return $responder->getResponse();
	}

	/**
	 * @return Action
	 */
	public function createAction(Responder $responder, $name) {
		if (!isset($this->actionFactories[$name])) {
			throw new ActionNotFound;
		}

		return $this->actionFactories[$name]->createAction($responder, $this->config);
	}

}
