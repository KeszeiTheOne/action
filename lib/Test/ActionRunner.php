<?php

namespace Keszei\Action\Test;

use Keszei\Action\ConfigSpy;
use Keszei\Action\Model\Action;
use Keszei\Action\Model\ActionFactory;
use Keszei\Action\Model\Request;
use Keszei\Action\Model\Response;
use Keszei\Action\Responder;

class ActionRunner {

	/**
	 * @return Response
	 */
	public static function runAction(Request $request, ActionFactory $factory, array $methods = [], Responder $responder = null) {
		if (null === $responder) {
			$responder = new Responder();
		}
		$action = static::createAction($factory, $methods, $responder);
		$action->run($request);

		return $responder->getResponse();
	}

	/**
	 * @return Action
	 */
	public static function createAction(ActionFactory $factory, array $methods = [], Responder $responder = null) {
		return $factory->createAction($responder, static::createConfigSpy($methods));
	}

	public static function createConfigSpy(array $methods = []) {
		$config = new ConfigSpy();
		foreach ($methods as $method => $callback) {
			$config->addMethod($method, $callback);
		}

		return $config;
	}

}
