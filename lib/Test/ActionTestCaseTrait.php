<?php

namespace Keszei\Action\Test;

use Exception;
use Keszei\Action\Exception\UnexpectedType;
use Keszei\Action\Model\Response;
use Keszei\Action\Test\Fixtures\RequestDummy;

trait ActionTestCaseTrait {

	protected function assertRunThrowsUnexpectedTypeWhenRequestIsUnknown() {
		$this->assertRunThrows(new RequestDummy(), UnexpectedType::class);
	}

	protected function assertRunThrows($request, $expectedException) {
		try {
			$this->runAction($request);
		}
		catch (Exception $exc) {
			$this->assertInstanceOf($expectedException, $exc, $exc->getMessage());
			return $exc;
		}

		$this->fail("$expectedException should be thrown.");
	}

	/**
	 * @return Response
	 */
	abstract protected function runAction($request);
}
