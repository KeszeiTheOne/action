<?php

namespace Keszei\Action\Decorator;

use Keszei\Action\Model\ActionFactory;
use Keszei\Action\Model\Config;
use Keszei\Action\Responder;

class CallbackActionFactory implements ActionFactory {

	private $callback;

	public function __construct($callback) {
		$this->callback = $callback;
	}

	public function createAction(Responder $respomder, Config $config) {
		return call_user_func($this->callback, $respomder, $config);
	}

}
