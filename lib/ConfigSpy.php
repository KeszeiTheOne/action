<?php

namespace Keszei\Action;

use Keszei\Action\Model\Config;

class ConfigSpy implements Config {

	private $methods = [];

	public function addMethod($name, $callback) {
		$this->methods[$name] = $callback;
	}

	public function __call($name, $arguments) {
		return call_user_func_array($this->methods[$name], $arguments);
	}

}
