<?php

namespace Keszei\Action\Model;

use Keszei\Action\Responder;

interface ActionFactory {

	public function createAction(Responder $responder, Config $config);
}
