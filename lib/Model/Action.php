<?php

namespace Keszei\Action\Model;

interface Action {

	public function run(Request $request);
}
