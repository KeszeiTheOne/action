<?php

namespace Keszei\Action\Model;

interface ActionFactoryPackage {

	public function getActionFactories();
}
