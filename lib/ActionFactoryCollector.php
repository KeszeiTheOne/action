<?php

namespace Keszei\Action;

use ArrayAccess;
use ArrayIterator;
use IteratorAggregate;
use Keszei\Action\Model\ActionFactoryPackage;

class ActionFactoryCollector implements IteratorAggregate {

	private $actionFactories = [];

	public function __construct(array $actionFactories) {
		foreach ($actionFactories as $name => $actionFactory) {
			$this->addActionFactory($name, $actionFactory);
		}
	}

	public function addActionFactory($name, $actionFactory) {
		$this->actionFactories[$name] = $actionFactory;
	}

	public function getIterator() {
		$collected = new ArrayIterator();

		foreach ($this->actionFactories as $name => $actionFactory) {
			$this->aggregateActionFactory($collected, $name, $actionFactory);
		}

		return $collected;
	}

	private function aggregateActionFactory(ArrayAccess $collected, $name, $actionFactory) {
		if ($actionFactory instanceof ActionFactoryPackage) {
			foreach ($actionFactory->getActionFactories() as $packageName => $packageActionFactory) {
				$this->aggregateActionFactory($collected, $name . "." . $packageName, $packageActionFactory);
			}
		}
		else {
			$collected->offsetSet($name, $actionFactory);
		}
	}

}
