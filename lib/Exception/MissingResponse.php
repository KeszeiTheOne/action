<?php

namespace Keszei\Action\Exception;

use RuntimeException;

class MissingResponse extends RuntimeException {
	
}
