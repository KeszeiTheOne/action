<?php

namespace Keszei\Action\Exception;

use Keszei\Action\Model\Request;
use RuntimeException;

class InvalidRequest extends RuntimeException {

	/**
	 * @var Request
	 */
	private $request;

	public function __construct(Request $request) {
		$this->request = $request;
	}

	public function getRequest() {
		return $this->request;
	}

}
