<?php

namespace Keszei\Action\Exception;

use RuntimeException;

class ActionNotFound extends RuntimeException {
	
}
