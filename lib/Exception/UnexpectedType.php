<?php

namespace Keszei\Action\Exception;

use RuntimeException;

class UnexpectedType extends RuntimeException {
	
}
